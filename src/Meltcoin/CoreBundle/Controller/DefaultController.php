<?php

namespace Meltcoin\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MeltcoinCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
