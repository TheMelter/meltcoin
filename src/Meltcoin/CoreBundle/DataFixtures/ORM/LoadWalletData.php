<?php
namespace Meltcoin\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Meltcoin\CoreBundle\Entity\Wallet;

class LoadWalletData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $wallet = new Wallet();
        $wallet->setUser($this->getReference('user'));
        $this->addReference('wallet', $wallet);

        $manager->persist($wallet);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
