<?php
namespace Meltcoin\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Meltcoin\CoreBundle\Entity\Api;

class LoadApiData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $api = new Api();
        $api->setName('BTC-E');
        $api->setDescription('Api for https://www.btc-e.com');
        $api->setKey('9J03STS8-6E1N3XU8-WFQSMD89-1PDHICUI-11GOVKFO');
        $api->setSecret('c60e6e4bcea4fed75f9daa70e1304aa43a73fcfcc74d8c3bb4d111652b5f9716');
        $api->setUser($this->getReference('user'));
        $this->addReference('api', $api);

        $manager->persist($api);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
