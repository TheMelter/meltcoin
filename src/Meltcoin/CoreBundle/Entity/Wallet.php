<?php
namespace Meltcoin\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Meltcoin\CoreBundle\Repository\WalletRepository")
 * @ORM\Table(name="wallet")
 */
class Wallet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="usd", type="decimal", precision=12, scale=7)
     */
    protected $usd;

    /**
     * @ORM\Column(name="btc", type="decimal", precision=12, scale=7)
     */
    protected $btc;

    /**
     * @ORM\Column(name="ltc", type="decimal", precision=12, scale=7)
     */
    protected $ltc;

    /**
     * @ORM\Column(name="nmc", type="decimal", precision=12, scale=7)
     */
    protected $nmc;

    /**
     * @ORM\Column(name="rur", type="decimal", precision=12, scale=7)
     */
    protected $rur;

    /**
     * @ORM\Column(name="eur", type="decimal", precision=12, scale=7)
     */
    protected $eur;

    /**
     * @ORM\Column(name="nvc", type="decimal", precision=12, scale=7)
     */
    protected $nvc;

    /**
     * @ORM\Column(name="trc", type="decimal", precision=12, scale=7)
     */
    protected $trc;

    /**
     * @ORM\Column(name="ppc", type="decimal", precision=12, scale=7)
     */
    protected $ppc;

    /**
     * @ORM\Column(name="ftc", type="decimal", precision=12, scale=7)
     */
    protected $ftc;

    /**
     * @ORM\Column(name="xpm", type="decimal", precision=12, scale=7)
     */
    protected $xpm;

    /**
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     */
    protected $updateTime;

    /**
     * @ORM\OneToOne(targetEntity="Meltcoin\CoreBundle\Entity\User", inversedBy="wallet")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->usd = 0;
        $this->btc = 0;
        $this->ltc = 0;
        $this->nmc = 0;
        $this->rur = 0;
        $this->eur = 0;
        $this->nvc = 0;
        $this->trc = 0;
        $this->ppc = 0;
        $this->ftc = 0;
        $this->xpm = 0;
        $this->updateTime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usd
     *
     * @param string $usd
     * @return Wallet
     */
    public function setUsd($usd)
    {
        $this->usd = $usd;

        return $this;
    }

    /**
     * Get usd
     *
     * @return string 
     */
    public function getUsd()
    {
        return $this->usd;
    }

    /**
     * Set btc
     *
     * @param string $btc
     * @return Wallet
     */
    public function setBtc($btc)
    {
        $this->btc = $btc;

        return $this;
    }

    /**
     * Get btc
     *
     * @return string 
     */
    public function getBtc()
    {
        return $this->btc;
    }

    /**
     * Set ltc
     *
     * @param string $ltc
     * @return Wallet
     */
    public function setLtc($ltc)
    {
        $this->ltc = $ltc;

        return $this;
    }

    /**
     * Get ltc
     *
     * @return string 
     */
    public function getLtc()
    {
        return $this->ltc;
    }

    /**
     * Set nmc
     *
     * @param string $nmc
     * @return Wallet
     */
    public function setNmc($nmc)
    {
        $this->nmc = $nmc;

        return $this;
    }

    /**
     * Get nmc
     *
     * @return string 
     */
    public function getNmc()
    {
        return $this->nmc;
    }

    /**
     * Set rur
     *
     * @param string $rur
     * @return Wallet
     */
    public function setRur($rur)
    {
        $this->rur = $rur;

        return $this;
    }

    /**
     * Get rur
     *
     * @return string 
     */
    public function getRur()
    {
        return $this->rur;
    }

    /**
     * Set eur
     *
     * @param string $eur
     * @return Wallet
     */
    public function setEur($eur)
    {
        $this->eur = $eur;

        return $this;
    }

    /**
     * Get eur
     *
     * @return string 
     */
    public function getEur()
    {
        return $this->eur;
    }

    /**
     * Set nvc
     *
     * @param string $nvc
     * @return Wallet
     */
    public function setNvc($nvc)
    {
        $this->nvc = $nvc;

        return $this;
    }

    /**
     * Get nvc
     *
     * @return string 
     */
    public function getNvc()
    {
        return $this->nvc;
    }

    /**
     * Set trc
     *
     * @param string $trc
     * @return Wallet
     */
    public function setTrc($trc)
    {
        $this->trc = $trc;

        return $this;
    }

    /**
     * Get trc
     *
     * @return string 
     */
    public function getTrc()
    {
        return $this->trc;
    }

    /**
     * Set ppc
     *
     * @param string $ppc
     * @return Wallet
     */
    public function setPpc($ppc)
    {
        $this->ppc = $ppc;

        return $this;
    }

    /**
     * Get ppc
     *
     * @return string 
     */
    public function getPpc()
    {
        return $this->ppc;
    }

    /**
     * Set ftc
     *
     * @param string $ftc
     * @return Wallet
     */
    public function setFtc($ftc)
    {
        $this->ftc = $ftc;

        return $this;
    }

    /**
     * Get ftc
     *
     * @return string 
     */
    public function getFtc()
    {
        return $this->ftc;
    }

    /**
     * Set xpm
     *
     * @param string $xpm
     * @return Wallet
     */
    public function setXpm($xpm)
    {
        $this->xpm = $xpm;

        return $this;
    }

    /**
     * Get xpm
     *
     * @return string 
     */
    public function getXpm()
    {
        return $this->xpm;
    }

    /**
     * Set updateTime
     *
     * @param \DateTime $updateTime
     * @return Wallet
     */
    public function setUpdateTime($updateTime)
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * Get updateTime
     *
     * @return \DateTime 
     */
    public function getUpdateTime()
    {
        return $this->updateTime;
    }

    /**
     * Set user
     *
     * @param \Meltcoin\CoreBundle\Entity\User $user
     * @return Wallet
     */
    public function setUser(\Meltcoin\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Meltcoin\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
