<?php
namespace Meltcoin\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Meltcoin\CoreBundle\Repository\UserRepository")
 * @ORM\Table(name="app_user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(name="name_first", type="string", length=50)
     */
    protected $firstname;

    /**
     * @ORM\Column(name="name_last", type="string", length=50)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dob;

    /**
     * @ORM\Column(name="creation_time", type="datetime", nullable=true)
     */
    protected $creationTime;

    /**
     * @ORM\OneToOne(targetEntity="Meltcoin\CoreBundle\Entity\Wallet", mappedBy="user")
     */
    protected $wallet;

    /**
     * @ORM\OneToOne(targetEntity="Meltcoin\CoreBundle\Entity\Api", mappedBy="user")
     */
    protected $api;

    /**
     * Constructor.
     */ 
    public function __construct()
    {
        $this->creationTime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime 
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set creationTime
     *
     * @param \DateTime $creationTime
     * @return User
     */
    public function setCreationTime($creationTime)
    {
        $this->creationTime = $creationTime;

        return $this;
    }

    /**
     * Get creationTime
     *
     * @return \DateTime 
     */
    public function getCreationTime()
    {
        return $this->creationTime;
    }

    /**
     * Set balance
     *
     * @param string $balance
     * @return User
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set wallet
     *
     * @param \Meltcoin\CoreBundle\Entity\Wallet $wallet
     * @return User
     */
    public function setWallet(\Meltcoin\CoreBundle\Entity\Wallet $wallet = null)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * Get wallet
     *
     * @return \Meltcoin\CoreBundle\Entity\Wallet 
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * Set api
     *
     * @param \Meltcoin\CoreBundle\Entity\Api $api
     * @return User
     */
    public function setApi(\Meltcoin\CoreBundle\Entity\Api $api = null)
    {
        $this->api = $api;

        return $this;
    }

    /**
     * Get api
     *
     * @return \Meltcoin\CoreBundle\Entity\Api 
     */
    public function getApi()
    {
        return $this->api;
    }
}
