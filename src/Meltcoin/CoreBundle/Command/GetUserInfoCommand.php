<?php
namespace Meltcoin\CoreBundle\Command;

use Meltcoin\CoreBundle\BTCE\UserInfoRequest;

class GetUserInfoCommand extends BaseCommand
{
    /**
     * {@inheritDoc}
     */
    protected function init()
    {
        $this->commandName = 'mc:user:info';
        $this->commandDescription = 'Retrieves user info from BTC-E';
    }

    /**
     * {@inheritDoc}
     */
    public function executeCommand() 
    {
        $key = $this->api->getKey();
        $secret = $this->api->getSecret();

        $request = new UserInfoRequest($key, $secret);
        $response = $request->send();

        $response = json_decode($response, true);

        print_r($response);
    }
}
