<?php
namespace Meltcoin\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends ContainerAwareCommand
{
    protected $em;
    protected $commandName;
    protected $commandDescription;
    protected $rollingWindow;
    protected $baseUrl;
    protected $urlPattern;
    protected $dialog;
    protected $output;
    protected $input;
    protected $rootDir;
    protected $user;
    protected $api;

    const USERNAME = 'TheMelter';
    const API_NAME = 'BTC-E';

    /**
     * Initializes member variables
     * Need to override commandName & commandDescription in derived classes.
     */
    protected function init()
    {
        $this->rollingWindow = 100;
        $this->urlPattern = "\(\)A-Za-z0-9_\-";
        $this->baseUrl = "https://btc-e.com/tapi";
    }

    /**
     * Initializes services once executed
     */
    protected function initializeServices()
    {
        $this->em = $this->get('doctrine.orm.entity_manager');
        $this->dialog = $this->getHelperSet()->get('dialog');
        $this->rootDir = $this->get('kernel')->getRootDir() . "/../";
        $this->user = $this->getUser(self::USERNAME);
        $this->api = $this->getApi(self::API_NAME);
    }

    /**
     * Shortcut for getting container services
     */
    protected function get($service)
    {
        return $this->getContainer()->get($service);
    }

    /** 
     * Configures command
     */
    protected function configure()
    {
        $this->init();

        $this
            ->setName($this->commandName)
            ->setDescription($this->commandDescription)
        ;
    }

    /**
     * Executes command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;
        $this->initializeServices();
        $this->postServiceInit();
        $this->executeCommand();
    }

    /**
     * Function executed right after services are initialized.
     */
    protected function postServiceInit() {}

    /**
     * Executes command. Created so that it can be overriden without parameters.
     */
    protected function executeCommand() {}

    /**
     * Returns User repository.
     */
    protected function getUserRepository() 
    {
        return $this->em->getRepository('MeltcoinCoreBundle:User');
    }

    /**
     * Returns Api repository.
     */
    protected function getApiRepository() 
    {
        return $this->em->getRepository('MeltcoinCoreBundle:Api');
    }

    /**
     * Returns BTC-E api entity.
     */
    protected function getApi($name)
    {
        return $this->getApiRepository()->findOneByName($name);
    }

    /**
     * Returns main user entity.
     */
    protected function getUser($username)
    {
        return $this->getUserRepository()->findOneByUsername($username);
    }

    /**
     * Callback function for multi_curl.
     * Processes data.
     */
    protected function processData($content, $url, $extra = null) {}

    /**
     * Parallel curls
     */
    protected function multi_curl($urls, $callback, $extras = null, $custom_options = null)
    {
        $rolling_window = $this->rollingWindow;
        $rolling_window = (count($urls) < $rolling_window) ? count($urls) : $rolling_window;

        $master = curl_multi_init();
        $curl_arr = array();

        $std_options = array(
            CURLOPT_RETURNTRANSFER  =>  true,
            CURLOPT_FOLLOWLOCATION  =>  0,
		    CURLOPT_BINARYTRANSFER  =>  true,
		    CURLOPT_COOKIESESSION   =>  true,
            CURLOPT_MAXREDIRS       =>  5,
            CURLOPT_USERAGENT       =>  'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1',
            CURLOPT_IPRESOLVE       =>  CURL_IPRESOLVE_V4,
		    CURLOPT_FRESH_CONNECT   =>  true,
        );
        $options = ($custom_options) ? ($std_options + $custom_options) : $std_options;

        for ($i = 0; $i < $rolling_window; ++$i) {
            $ch = curl_init();
            $options[CURLOPT_URL] = $urls[$i];
            curl_setopt_array($ch, $options);
            curl_multi_add_handle($master, $ch);
        }

        do {
            while (($execrun = curl_multi_exec($master, $running)) == CURLM_CALL_MULTI_PERFORM);
            if ($execrun != CURLM_OK)
                break;

            while ($done = curl_multi_info_read($master)) {
                $info = curl_getinfo($done['handle']);
                $current_url = $info['url'];

                if ($info['http_code'] == 200) {
                    $output = curl_multi_getcontent($done['handle']);
                    $this->$callback($output, $current_url, $extras[strtolower($current_url)]);
                }
                elseif ($info['http_code'] == 301 || $info['http_code'] == 302) {
                    // Only redirect once. Ain't nobody got time for that.
                    $redirected_url = $info['redirect_url'];
                    $output = $this->curl($redirected_url);

                    if ($output) {
                        // current_url is original url.
                        $this->$callback($output, $redirected_url, $extras[strtolower($current_url)]);
                    }
                }
                else {
                    // Handle error
                    // Try once more.
                    $output = $this->curl($current_url);
                    if ($output) {
                        $this->$callback($output, $current_url, $extras[strtolower($current_url)]);
                    }
                    else {
                        print_r($info);
                        $logger = $this->get('logger');
                        $logger->err("Error when curling: $current_url");
                    }
                }

                if (count($urls) > $i) {
                    $ch = curl_init();
                    $options[CURLOPT_URL] = $urls[$i++];
                    curl_setopt_array($ch, $options);
                    curl_multi_add_handle($master, $ch);
                }

                curl_multi_remove_handle($master, $done['handle']);
            }
        } while ($running);

        curl_multi_close($master);
        return true;
    }

    /**
     * Regular curls. Used for redirected urls.
     */
    protected function curl($url)
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($info['http_code'] == 200) {
            return $output;
        }

        return null;
    }

    /**
     * Parses input url's with semicolon as delimiter.
     */
    protected function parseInputUrl($url)
    {
        $urls = explode(';', $url);
        if (trim(end($urls)) == "") {
            array_pop($urls);
        }
        return $urls;
    }

    /**
     * Returns entity class name.
     */
    protected function getEntityClassName($entity)
    {
        $classArr = explode('\\', get_class($entity));
        return end($classArr);
    }
}
