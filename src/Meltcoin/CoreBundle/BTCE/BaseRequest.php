<?php
namespace Meltcoin\CoreBundle\BTCE;

abstract class BaseRequest
{
    protected $method;
    protected $query;
    protected $secret;
    protected $key;
    protected $baseUrl;

    public function __construct($key, $secret)
    {
        $this->query = array();
        $this->baseUrl = "https://btc-e.com/tapi";

        $this->key = $key;
        $this->secret = $secret;

        $this->setNonce();
        $this->init();
    }

    /**
     * Initialize.
     */
    abstract protected function init();

    /**
     * Sends data.
     */
    public function send()
    {
        return $this->curl($this->baseUrl, $this->buildQuery(), $this->getHeaders());
    }

    /**
     * Curl.
     */
    protected function curl($url, $query, $headers)
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $output = curl_exec($ch); 
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($info['http_code'] == 200) {
            return $output;
        }

        return null;
    }

    /**
     * Builds query from array to string.
     */
    protected function buildQuery()
    {
        return http_build_query($this->query, '', '&');
    }

    /**
     * Returns signed headers.
     */
    protected function getHeaders()
    {
        return array(
            'Sign: ' . $this->signQuery(),
            'Key: ' . $this->key,
        );
    }

    /**
     * Signs and returns query string.
     */
    protected function signQuery()
    {
        return hash_hmac('sha512', $this->buildQuery(), $this->secret);
    }

    /**
     * Sets method.
     */
    protected function setMethod($method)
    {
        $this->query['method'] = $method;
    }

    /**
     * Sets nonce.
     */
    protected function setNonce()
    {
        $nonce = explode(' ', microtime());
        $this->query['nonce'] = $nonce[1];
    }
}
