<?php
namespace Meltcoin\CoreBundle\BTCE;

class UserInfoRequest extends BaseRequest
{
    /**
     * {@inheritDoc}
     */
    protected function init()
    {
        $this->setMethod('getInfo');
    }
}
